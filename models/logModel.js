const mongoose = require('mongoose');

const logSchema = new mongoose.Schema({
  message: {
    type: String,
    require: true,
  },
  time: {
    type: Date,
    default: Date.now(),
  },

});

module.exports.Log = mongoose.model('logs', logSchema);
