const express = require('express');
const {isUserIsShipper, isUserIsDriver} = require('../middleware/userRole');
const activeLoad = require('../middleware/activeLoadMiddleware');
const loadById = require('../middleware/loadByIdMiddleware');
const getLoads = require('../controllers/getLoads');
const addLoad = require('../controllers/addLoad');
const getLoadActive = require('../controllers/getLoadActive');
const nextStateLoad = require('../controllers/nextStateLoad');
const getLoad = require('../controllers/getLoad');
const updateLoad = require('../controllers/updateLoad');
const removeLoad = require('../controllers/removeLoad');
const postLoad = require('../controllers/postLoad');
const getShippingInfo = require('../controllers/getShippingInfo');
const loadRouter = new express.Router();

loadRouter.get('/', getLoads);
loadRouter.post('/', isUserIsShipper, addLoad);
loadRouter.get('/active', isUserIsDriver, activeLoad, getLoadActive);
loadRouter.patch('/active/state', isUserIsDriver, activeLoad, nextStateLoad);
loadRouter.get('/:id', isUserIsShipper, loadById, getLoad);
loadRouter.put('/:id', isUserIsShipper, loadById, updateLoad);
loadRouter.delete('/:id', isUserIsShipper, loadById, removeLoad);
loadRouter.post('/:id/post', isUserIsShipper, loadById, postLoad);
loadRouter.get('/:id/shipping_info', isUserIsShipper,
    loadById, getShippingInfo);

module.exports = loadRouter;
