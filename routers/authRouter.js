const express = require('express');
const userIsExists = require('../middleware/userIsExistsMiddlware');
const userRegistration = require('../controllers/userRegistration');
const userLogin = require('../controllers/userLogin');
const validationMiddleware = require('../middleware/validationMiddleware');
const forgotPassword = require('../controllers/forgotPassword');
const asyncWrapper = require('../routers/heplers/asyncWrapper');
const authRouter = new express.Router();


authRouter.post('/register',
    asyncWrapper(validationMiddleware), userRegistration);
authRouter.post('/login', userIsExists, userLogin);
authRouter.post('/forgot_password', userIsExists, forgotPassword);

module.exports = authRouter;
