const express = require('express');
const getTrucks = require('../controllers/getTrucks');
const addTruck = require('../controllers/addTrcuk');
const getTruck = require('../controllers/getTruck');
const updateTruck = require('../controllers/updateTruck');
const removeTruck = require('../controllers/removeTruck');
const assignTruck = require('../controllers/assignTruck');
const truckValidation = require('../middleware/truckValidationMiddleware');
const takeTruckById = require('../middleware/takeTruckByIdMiddleware');

const truckRouter = new express.Router();

truckRouter.get('/', getTrucks);
truckRouter.post('/', truckValidation, addTruck);
truckRouter.get('/:id', takeTruckById, getTruck);
truckRouter.put('/:id', takeTruckById, updateTruck);
truckRouter.delete('/:id', takeTruckById, removeTruck);
truckRouter.post('/:id/assign', takeTruckById, assignTruck);

module.exports = truckRouter;
