const isUserIsDriver = (req, res, next) => {
  if (req.user.role === 'DRIVER') {
    next();
  } else {
    res.status(400).json({message: 'Only drivers can watch this page'});
  }
};

const isUserIsShipper = (req, res, next) => {
  if (req.user.role === 'SHIPPER') {
    next();
  } else {
    res.status(400).json({message: 'Only shippers can watch this page'});
  }
};

module.exports = {
  isUserIsDriver,
  isUserIsShipper,
};
