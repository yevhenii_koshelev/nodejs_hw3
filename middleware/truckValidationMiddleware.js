const truckSchema = require('../validationSchemes/truckSchema');

module.exports = truckValidationMiddleware = async (req, res, next) => {
  try {
    await truckSchema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};
