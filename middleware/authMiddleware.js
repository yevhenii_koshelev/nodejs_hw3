const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET;

module.exports = authMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];
  if (!header) {
    return res.status(400).json({
      message: `Non authorization http header found!`,
    });
  }

  const [tokenType, token] = header.split(' ');
  if (!token || !tokenType) {
    return res.status(400).json({message: `Non JWT token found!`});
  }

  req.user = jwt.verify(token, JWT_SECRET);
  next();
};
