const {Truck} = require('../models/truckModel');

module.exports = takeTruckById = async (req, res, next) => {
  const userId = req.user.id;
  const {id} = req.params;
  try {
    const truck = await Truck.findOne({created: userId, _id: id});

    if (!truck) {
      return res.status(400).json({message: 'Bad request'});
    }
    req.truck = truck;
    next();
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
