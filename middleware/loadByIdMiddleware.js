const {Load} = require('../models/loadModel');

module.exports = loadByIdMiddleware = async (req, res, next) => {
  const {user} = req;
  const {id} = req.params;
  let load;

  if (user.role === 'SHIPPER') {
    load = await Load.findOne({created: user.id, _id: id}, {__v: 0});
  } else if (user.role === 'DRIVER') {
    load = await Load.findOne({assigned: user.id, _id: id}, {__v: 0});
  }

  if (!load) {
    return res.status(400).json({message: 'Have no load with this id'});
  }

  req.load = load;
  next();
};
