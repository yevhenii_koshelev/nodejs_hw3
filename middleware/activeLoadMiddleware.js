const {Load} = require('../models/loadModel');

module.exports = activeLoadMiddleware = async (req, res, next) => {
  const {id} = req.user;
  try {
    const activeLoad = await Load.findOne({
      assigned: id,
      status: {$in: ['ASSIGNED']},
    });
    if (!activeLoad) {
      return res.status(200).json({message: 'You dont have active load now!'});
    }
    req.activeLoad = activeLoad;
    next();
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
