const userSchema = require('../validationSchemes/userSchema');

module.exports = validationMiddleware = async (req, res, next) => {
  try {
    await userSchema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};
