const {User} = require('../models/userModel');

module.exports = userIsExistsMiddleware = async (req, res, next) => {
  const {email} = req.body;
  try {
    const user = await User.findOne({'email': email});
    if (!user) {
      throw new Error();
    }

    req.user = user;
    next();
  } catch (err) {
    res.status(400).json({
      message: `No user with email ${email} found`,
    });
  }
};
