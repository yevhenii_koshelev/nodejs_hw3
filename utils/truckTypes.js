module.exports = truckTypes = {
  'SPRINTER': {
    length: 300,
    width: 250,
    height: 170,
    price: 1700,
  },
  'SMALL STRAIGHT': {
    length: 500,
    width: 250,
    height: 170,
    price: 2500,
  },
  'LARGE STRAIGHT': {
    length: 700,
    width: 350,
    height: 250,
    price: 4000,
  },
};
