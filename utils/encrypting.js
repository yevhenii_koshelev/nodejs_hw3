const bcrypt = require('bcrypt');

const hashingPassword = async (password) => {
  return await bcrypt.hash(password, 10);
};

const comparisonPassword = async (firstPass, secondPass) => {
  return await bcrypt.compare(firstPass, secondPass);
};


module.exports = {hashingPassword, comparisonPassword};
