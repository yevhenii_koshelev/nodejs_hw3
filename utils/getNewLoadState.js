const loadTypes = require('../utils/loadTypes');

module.exports = getNewLoadState = (state) => {
  const stateId = loadTypes.indexOf(state);
  state = {newState: loadTypes[stateId + 1]};

  if (stateId === loadTypes.length - 1) {
    state.done = true;
  }

  return state;
};
