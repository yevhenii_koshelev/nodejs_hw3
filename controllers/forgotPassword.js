module.exports = forgotPassword = async (req, res) => {
  try {
    // an email should be sent here, but not in this project
    res.status(200).json({message: 'New password sent to your email address'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
