const getNewLoadState = require('../utils/getNewLoadState');
const {Log} = require('../models/logModel');
const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');

module.exports = nextStateLoad = async (req, res) => {
  const {activeLoad, user} = req;
  const newState = getNewLoadState(activeLoad.state);

  try {
    const log = new Log({message: `Load state was changed to ${newState}`});
    await log.save();

    await Load.updateOne(activeLoad, {
      $set: {state: newState},
      $push: {logs: log},
    });

    if (newState.done) {
      const log = new Log({
        message: `Load status was changed to SHIPPED`,
      });
      await log.save();
      await Load.updateOne({_id: activeLoad.id}, {
        $set: {status: 'SHIPPED'},
        $push: {logs: log},
      });
      await Truck.updateOne(
          {assigned: user.id}, {$set: {status: 'IS'}});
    }

    res.status(200).json({message: `Load state changed to '${newState}'`});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
