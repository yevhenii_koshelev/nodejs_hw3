module.exports = getActiveLoad = async (req, res) => {
  const {activeLoad} = req;
  res.status(200).json({load: activeLoad});
};


