const {Log} = require('../models/logModel');
const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const loadTypes = require('../utils/loadTypes');

module.exports = postLoad = async (req, res) => {
  const {load} = req;
  const {payload} = load;
  const {length, width, height} = load.dimensions;
  const log = new Log({
    message: `Load status was changed to POSTED`,
  });

  try {
    if (load.status !== 'NEW') {
      return res.status(400).json(
          {message: 'Only loads with "NEW" status can be posted'});
    }
    await log.save();
    await Load.findOneAndUpdate({_id: req.load.id}, {
      $set: {status: 'POSTED'},
      $push: {logs: log},
    });

    const availableTruck = await Truck.findOne({
      status: {$eq: 'IS'},
      assigned: {$ne: null},
      price: {$gt: payload},
      length: {$gt: length},
      width: {$gt: width},
      height: {$gt: height},
    });

    console.log(availableTruck);

    if (!availableTruck) {
      const newLog = new Log({
        message: `Truck wasn't found. Load status was changed to NEW`,
      });
      await newLog.save();
      await Load.updateOne({_id: load.id}, {
        $set: {status: 'NEW'},
        $push: {logs: newLog},
      });
      return res.status(200).json({message: 'No available Trucks' +
            ' were found. Try again later'});
    }

    const newLog = new Log({
      message: `Truck was found. Load assigned for driver
     ${availableTruck.assigned}`,
    });
    await newLog.save();
    await Load.updateOne({_id: load.id}, {
      $set: {
        status: 'ASSIGNED',
        assigned_to: availableTruck.assigned,
        state: loadTypes[0],
      },
      $push: {logs: newLog},
    });
    await Truck.updateOne(availableTruck, {$set: {status: 'OL'}});

    res.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
