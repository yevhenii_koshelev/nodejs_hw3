const {hashingPassword} = require('../utils/encrypting');
const {User} = require('../models/userModel');

module.exports = userRegistration = async (req, res) => {
  const {email, password, role} = req.body;
  try {
    const hashPassword = await hashingPassword(password);
    const user = new User({
      email,
      password: hashPassword,
      role,
    });
    await user.save();

    res.status(200).json({
      message: `User created successfully!`,
    });
  } catch (err) {
    if (err.name === 'ValidationError') {
      return res.status(400).json({message: err});
    } else if (err.name === 'MongoError' && err.code === 11000) {
      return res.status(400).json({message: 'Email must be unique'});
    }
    res.status(500).json({message: err});
  }
};
