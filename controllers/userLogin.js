const {comparisonPassword} = require('../utils/encrypting');
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET;

module.exports = userLogin = async (req, res) => {
  const {password} = req.body;
  const {user} = req;
  try {
    const comparePassword = await comparisonPassword(password, user.password);

    if (!comparePassword) {
      return res.status(400).json({message: `Wrong password`});
    }

    const token = jwt.sign({
      email: user.email,
      id: user._id,
      role: user.role,
    }, JWT_SECRET);
    res.status(200).json({message: `Login successfully`, jwt_token: token});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
