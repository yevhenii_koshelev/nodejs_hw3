const {User} = require('../models/userModel');

module.exports = getUserInfo = async (req, res) => {
  const {id} = req.user;
  try {
    const user = await User.findById(id, '_id email createDate');
    res.status(200).json({user});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
