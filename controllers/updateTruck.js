const truckTypes = require('../utils/truckTypes');

module.exports = updateTruck = async (req, res) => {
  const {truck} = req;
  if (truck.assigned_to) {
    return res.status(400).json({message: 'You cant update assigned truck'});
  }
  const {type} = req.body;
  const {length, width, height, price} = truckTypes[type];
  truck.type = type;
  truck.length = length;
  truck.width = width;
  truck.height = height;
  truck.price = price;
  try {
    await truck.save();
    res.status(200).json({message: 'Truck was updated successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
