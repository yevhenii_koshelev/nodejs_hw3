const {Truck} = require('../models/truckModel');
const truckTypes = require('../utils/truckTypes');

module.exports = addTruck = async (req, res) => {
  const {id} = req.user;
  const {type} = req.body;
  const {length, width, height, price} = truckTypes[type];

  const truck = new Truck({
    created: id,
    type,
    length,
    width,
    height,
    price,
  });

  try {
    await truck.save();
    res.status(200).json({message: 'Truck was created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
