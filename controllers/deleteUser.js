const {User} = require('../models/userModel');

module.exports = deleteUser = async (req, res) => {
  const {id} = req.user;
  try {
    await User.findByIdAndRemove(id);
    res.status(200).json({
      message: `Profile deleted successfully`,
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
