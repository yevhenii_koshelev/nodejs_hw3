module.exports = removeLoad = async (req, res) => {
  const {load} = req;
  try {
    if (load.status !== 'NEW') {
      throw new Error('Only loads with "NEW" status can be deleted');
    }
    await load.remove();
    res.status(200).json({message: 'Load was deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
