module.exports = removeTruck = async (req, res) => {
  const {truck} = req;
  try {
    await truck.delete();
    res.status(200).json({message: 'Truck was deleted successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
