const {Load} = require('../models/loadModel');

module.exports = getLoads = async (req, res) => {
  let loads;
  const isShipper = req.user.role === 'SHIPPER';
  const {offset, limit, status} = req.query;
  const requestOptions = {
    offset: +offset,
    limit: +limit,
  };

  try {
    if (isShipper) {
      loads = await Load.find(
          {created_by: req.user._id},
          {__v: 0},
          requestOptions);
    } else {
      loads = await Load.find(
          {assigned_to: req.user._id},
          {__v: 0},
          requestOptions);
    }
    if (status) {
      loads = loads.filter((load) => load.status === status);
    }
    res.status(200).json({loads});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
