const {Load} = require('../models/loadModel');

module.exports = updateLoad = async (req, res) => {
  const {load, body} = req;
  try {
    if (load.status !== 'NEW') {
      throw new Error('Only loads with "NEW" status can be updated');
    }
    await Load.updateOne(load, body);
    res.status(200).json({message: 'Your load was updated successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
