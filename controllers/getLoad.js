module.exports = getLoad = (req, res) => {
  const {load} = req;
  res.status(200).json({load});
};
