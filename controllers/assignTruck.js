module.exports = assignTruck = async (req, res) => {
  const {truck} = req;
  const {id} = req.user;
  truck.assigned = id;
  try {
    truck.save();
    res.status(200).json({message: 'Truck was assigned successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
