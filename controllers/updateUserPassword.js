const {User} = require('../models/userModel');
const {hashingPassword, comparisonPassword} = require('../utils/encrypting');

module.exports = updateUserPassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  try {
    const user = await User.findById(req.user.id);
    const comparePass = await comparisonPassword(oldPassword, user.password);

    if (!comparePass) {
      return res.status(400).json({message: `Wrong old password`});
    }
    const hashPassword = await hashingPassword(newPassword);
    user.password = hashPassword;
    await user.save();
    res.status(200).json({message: `Password changed successfully`});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
