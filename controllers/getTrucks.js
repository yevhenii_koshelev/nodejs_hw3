const {Truck} = require('../models/truckModel');

module.exports = getTrucks = async (req, res) => {
  const {id} = req.user;
  try {
    const trucks = await Truck.find({created: id}, {_v: 0});
    res.status(200).json({trucks});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
