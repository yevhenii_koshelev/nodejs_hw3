module.exports = getTruck = (req, res) => {
  const {truck} = req;
  res.status(200).json({truck});
};
