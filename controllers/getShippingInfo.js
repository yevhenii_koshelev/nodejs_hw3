const {Truck} = require('../models/truckModel');

module.exports = getShippingInfo = async (req, res) => {
  const {load} = req;
  if (!load.assigned) {
    return res.status(200).json({load: req.load});
  }
  try {
    const truck = await Truck.findOne({assigned: load.assigned});
    res.status(200).json({load, truck});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

