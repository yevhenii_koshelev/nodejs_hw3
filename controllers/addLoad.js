const {Load} = require('../models/loadModel');
const {Log} = require('../models/logModel');

module.exports = addLoad = async (req, res) => {
  const {id} = req.user;
  const {
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
  } = req.body;

  const log = new Log({message: 'Load was created'});
  const load = new Load({
    created: id,
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
    logs: [log],
  });

  try {
    await log.save();
    await load.save();
    res.status(200).json({message: 'Load was created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
