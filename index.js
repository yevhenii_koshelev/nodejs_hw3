require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');
const authMiddleware = require('./middleware/authMiddleware');
const {isUserIsDriver} = require('./middleware/userRole');
const {MONGOLINK, PORT} = process.env;

const app = express();

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/users', authMiddleware, userRouter);
app.use('/api/auth', authRouter);
app.use('/api/trucks', authMiddleware, isUserIsDriver, truckRouter);
app.use('/api/loads', authMiddleware, loadRouter);
app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});


const start = async () => {
  await mongoose.connect(MONGOLINK, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });


  app.listen(PORT, () => {
    console.log(`Server started at port: ${PORT}`);
  });
};

start();
