const Joi = require('joi');

module.exports = truckSchema = Joi.object({
  type: Joi.string()
      .pattern(new RegExp('(^SPRINTER$)|(^SMALL STRAIGHT$)|(^LARGE STRAIGHT$)'))
      .required(),
});
