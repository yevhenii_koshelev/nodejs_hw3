const Joi = require('joi');

module.exports = userSchema = Joi.object({
  email: Joi.string()
      .email()
      .required(),

  password: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
      .required(),

  role: Joi.string()
      .pattern(new RegExp('(^SHIPPER$)|(^DRIVER$)'))
      .required(),
});
